# PushAndTrigger

1) Clone this repo

2) Set up your actions as described here https://gitlab.com/howto-automated_deployment/pushandtrigger
    
3) update appsettings.json accordingly.

4) Add the location of the .exe to the system environment variable PATH.

5) Reload your shell.

6) Type `pull-and-trigger <projectname> <actionname> <branch>` 